#include <iostream>
#include <string>

using namespace std;

int countOccurrences(const string& substring, int number_of_strings) {
    int count = 0;
    for (int i = 0; i < number_of_strings; i++) {
        string match;
        cin >> match;

        size_t pos = match.find(substring);
        while (pos != string::npos) {
            count++;
            pos = match.find(substring, pos + 1);
        }
    }
    return count;
}

int main() {
    int number_of_strings = 3;

    cout << "Что ищем:" << endl;
    string substring;
    cin >> substring;

    cout << "Где ищем: " << endl;
    int count = countOccurrences(substring, number_of_strings);

    cout << count << endl;

    return 0;
}
