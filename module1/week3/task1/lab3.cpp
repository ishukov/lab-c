#include <iostream>
#include <sstream>
#include <map>
#include <iomanip>

using namespace std;

string getSensorDataInput() {
    string input;
    cout << "Enter sensor data: ";
    getline(cin, input);
    return input;
}

void parseSensorData(const string& sensorData, int& id, double& temperature) {
    id = stoi(sensorData.substr(0, 2));
    temperature = stoi(sensorData.substr(2));
}

map<int, pair<double, int>> calculateTemperatureAverages(const string& input) {    
    istringstream ss(input);
    string token;
    map<int, pair<double, int>> temp_map;

    while (getline(ss, token, '@')) {
        int id;
        double temperature;
        parseSensorData(token, id, temperature);

        if(temp_map.count(id)) {
            temp_map[id].first += temperature;
            temp_map[id].second++;
        } else {
            temp_map[id] = make_pair(temperature, 1);
        }
    }
    return temp_map;
}


void printTemperatureAverages(const map<int, pair<double, int>>& temp_map) {
    cout << "Sort by (id/temperature): "; 
    string sortBy;
    getline(cin, sortBy);

    bool sortByTemperature = false;
    if(sortBy == "temperature") {
        sortByTemperature = true;
    }

    if(sortByTemperature) {
        // Sort by temperature
        multimap<double, int> reverse_temp_map;
        for(const auto& [key, value]: temp_map) {
            double mean = value.first / value.second;
            reverse_temp_map.insert(make_pair(mean, key));
        }

        for(auto it = reverse_temp_map.rbegin(); it != reverse_temp_map.rend(); ++it) {
            int id = it->second;
            double mean = temp_map.at(id).first / temp_map.at(id).second;
            cout << id << " " << mean << "\n";
        }
    } else {
        // Sort by id
        for(const auto& [key, value]: temp_map) {
            int id = key;
            double mean = value.first / value.second;
            cout << id << " " << mean << "\n";
        }
    }
}

int main() {
    string input = getSensorDataInput();
    map<int, pair<double, int>> temp_map = calculateTemperatureAverages(input);
    cout << fixed << setprecision(1);
    printTemperatureAverages(temp_map);
    return 0;
}
