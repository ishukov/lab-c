#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

using namespace std;

double calculateEntropy(const string& input) {
    int charCount[256] = {0};
    int totalChars = 0;

    for (char c : input) {
        charCount[c]++;
        totalChars++;
    }

    double entropy = 0;
    for (int i = 0; i < 256; i++) {
        if (charCount[i] == 0) continue;
        double p = (double)charCount[i] / totalChars;
        entropy -= p * log2(p);
    }

    return entropy;
}

int main() {
    cout << "Ввод:" << endl;
    string input;
    getline(cin, input);

    double entropy = calculateEntropy(input);
    cout << "Вывод:" << endl;
    cout << fixed << setprecision(2) << entropy << endl;

    return 0;
}
