#include <iostream>
#include <string>
#include <regex>
using namespace std;

bool is_email_valid(string email) {
   return regex_match(email, regex ("\\w+@\\w+\\.\\w+"));
}

int main() {
    string email;
    getline(cin, email);

    if (is_email_valid(email)) {
        cout << "Да" << endl;
    }
    else {
        cout << "Нет" << endl;
    }
}
