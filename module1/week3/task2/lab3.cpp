#include <iostream>
#include <regex>

using namespace std;

string replace(string line) {
    return regex_replace(line, regex("(.)\\1{2,}"), "$1");
}

int main() {
    string line;
    getline(cin, line);
    replace(line);
    cout << replace(line) << endl;
    return 0;
}
