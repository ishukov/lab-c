#include <iostream>
using namespace std;

int main() {
  setlocale(LC_ALL, "Russian"); //русская локализация консоли
  string user;
  cout << "Введите имя пользователя: "; //cout - потоковый вывод данных
  cin >> user; //cin - потоковый ввод данных
  cout << "Привет," << user << "!\n" << endl;
  return 0;
}

