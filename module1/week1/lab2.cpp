#include <iostream>
#include <limits>
using namespace std;


int main() {
  //Шаблон numeric_limits предоставляет стандартизированный способ запроса различных арифметических типов данных
  //тип данных bool принимает два числовых значения: 1 и 0
  cout << "bool: " << numeric_limits<bool>::max() << " - max" << '\n';
  cout << "bool: " << numeric_limits<bool>::min() << " - min" << '\n';

  // short: представляет целое число в диапазоне от –32768 до 32767
  cout << "short: " << numeric_limits<short>::max() << " - max" << '\n';
  cout << "short: " << numeric_limits<short>::min() << " - min" << '\n';

  // Диапазон предельных значений int может варьироваться от –32768 до 32767 (при 2 байтах) или от −2 147 483 648 до 2 147 483 647 (при 4 байтах).
  cout << "int: " << numeric_limits<int>::max() << " - max" << '\n';
  cout << "int: " << numeric_limits<int>::min() << " - min" << '\n';

  // long: представляет целое число в диапазоне от −2 147 483 648 до 2 147 483 647
  cout << "long: " << numeric_limits<long>::max() << " - max" << '\n';
  cout << "long: " << numeric_limits<long>::min() << " - min" << '\n';

  // long double: представляет целое число в диапазоне от 3.3621e-4932  до 1.18973e+4932
  cout << "long double: " << numeric_limits<long double>::max() << " - max" << '\n';
  cout << "long double: " << numeric_limits<long double>::min() << " - min" << '\n';

  // float: представляет вещественное число ординарной точности с плавающей
  // точкой в диапазоне +/- 3.4E-38 до 3.4E+38
  cout << "float: " << numeric_limits<float>::max() << " - max" << '\n';
  cout << "float: " << numeric_limits<float>::min() << " - min" << '\n';

  // double: представляет вещественное число двойной точности с плавающей точкой
  // в диапазоне +/- 1.7E-308 до 1.7E+308
  cout << "double: " << numeric_limits<double>::max() << " - max" << '\n';
  cout << "double: " << numeric_limits<double>::min() << " - min" << '\n';

  return 0;
}

