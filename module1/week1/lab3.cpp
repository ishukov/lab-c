#include <iostream>
#include <math.h>

using namespace std;

int main() {
  float a, V;
  cout << "Введите значение длины ребра: ";
  cin >> a;
  V = ((float)5 / 12) * (3 + sqrt(5)) * pow(a, 3);
  cout << "Объём икосаэдра = " << V << endl;
  return 0;
}
