#include <iostream>
#include <string>

using namespace std;

int main()
{
    setlocale (LC_ALL, "Russian");
    string age;
    cout << "Введите целое число - возраст: ";
    cin >> age;
    if((age.find_first_not_of( "0123456789") == string::npos) == true) { // проверяется совпадение введённых данных с данными указынными в скобках 
        cout<<age<<" целое число"<<endl;
    }
    else {
        cout<<age<<" не целое число! Попробуйте снова..."<<endl;
        return 1;
    }
    int res = stoi(age); // преобразование строки в число
    switch(res % 10) { // последняя цифра числа определяет склонение
        case 1:
            cout<<"Введенный возраст: " << age << " год" << endl;
            break;
        case 2: case 3:  case 4: 
            cout<<"Введенный возраст: " << age << " года" << endl;
            break;
        default:
            cout<<"Введенный возраст: " << age << " лет" << endl;
            break;
    }
    return 0;
}
