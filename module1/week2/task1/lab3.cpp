#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    int day, temp, weather, wind, wetness;
    cout << "Ответьте на вопросы, которые определят хотите ли вы играть в бадминтон (Да/Нет)"<< endl;
    cout << "Какой  день  недели вы предпочитаете для игры в бадминтон от 1 до 7: " << endl;
    cin >> day;
    switch (day){
        case 1: case 2: case 3: case 4: case 5: case 6:
            cout << "Вам не нравится играть в бадминтон в этот день недели" << endl;
            cout << "Хотите ли вы играть в бадминтон - Нет" << endl;
            break;
        default:
            cout << "Вы ввели неверные данные" << endl;
            break;
        case 7:
            cout << "Какую температуру воздуха вы предпочитаете во время игры в бадминтон (1-жарко, 2-тепло, 3-холодно)? " << endl;
            cin >> temp;
            switch (temp) {
                case 3:
                    cout << "Вы не любите играть при такой температуре воздуха"<< endl;
                    cout << "Хотите ли вы играть в бадминтон - Нет" << endl;
                    break;
                default:
                    cout << "Вы ввели неверные данные" << endl;
                    break;
                case 1: case 2:
                cout << "Какой предпочтительный типы погоды (1-ясно, 2-облачно, 3-дождь, 4-снег, 5-град)"<< endl; 
                cin >> weather;
                switch (weather){
                    case 3: case 4: case 5:
                        cout << "Вы не любите играть в такой типы погоды"<< endl;
                        cout << "Хотите ли вы играть в бадминтон - Нет" << endl;
                        break;
                    default:
                        cout << "Вы ввели неверные данные" << endl;
                        break;
                    case 1: case 2:
                        cout << "Предпочитаете ли вы ветреную погоду при игре в бадминтон (1-Да, 2-Нет)?"<< endl;
                        cin >> wind;
                        switch (wind) {
                            case 1:
                                cout << "Вы не любите играть в ветрянную погоду"<< endl;
                                cout << "Хотите ли вы играть в бадминтон - Нет" << endl;
                                break;
                            default:
                                cout << "Вы ввели неверные данные" << endl;
                                break;
                            case 2:
                                cout << "Какую влажность воздуха вы предпочитаете при игре в бадминтон (1-высокую, 2-низкую)?" << endl; 
                                cin >> wetness;
                                switch (wetness) {
                                    case 1:
                                        cout << "Вы не любите играть при такой влажности воздуха"<< endl;
                                        cout << "Хотите ли вы играть в бадминтон - Нет" << endl;
                                        break;
                                    case 2:
                                        cout << "Хотите ли вы играть в бадминтон - Да" << endl;
                                        break;
                                    default:
                                        cout << "Вы ввели неверные данные" << endl;
                                        break;
                                }
                        }
                }
            }
    }
    return 0;
}
