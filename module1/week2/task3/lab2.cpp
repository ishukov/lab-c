#include<iostream>
using namespace std;

int Merge(int A[], int p, int q, int r)
{
    //Создать L <- A[p..q] and M <- A[q+1..r]
    int n1 = q - p + 1;
    int n2 =  r - q;
    int L[n1], M[n2];
    for (int i = 0; i < n1; i++)
        L[i] = A[p + i];
    for (int j = 0; j < n2; j++)
        M[j] = A[q + 1 + j];
    //Сохранить текущий индекс подмассивов и основной массив
    int i = 0, j = 0, k = p;
    //Пока не будет достигнут конец L или M, выбираются те, что больше среди элементов из L и M, и помещаются в нужной позиции в A[p..r]
    while (i < n1 && j < n2) {
        if (L[i] <= M[j]) {
            A[k] = L[i++];
        }
        else {
            A[k] = M[j++];
        }
        k++;
    }
    //Когда заканчиваются элементы в L или M, выбираются оставшиеся элементы и помещаются в A[p..r]
    while (i < n1) {
        A[k++] = L[i++];
    }
    while (j < n2) {
        A[k++] = M[j++];
    }
    return 0;
}
//Эта часть разделит массив на подмассивы, а затем объединит их, вызвав Merge()
int MergeSort(int A[],int p,int r) {
    int q;                                
    if(p<r) {
        q=(p+r)/2;
        MergeSort(A,p,q);
        MergeSort(A,q+1,r);
        Merge(A,p,q,r);
   }
   return 0;
}
int main()
{
    int n;
    cout<<"Введите размер массива: ";
    cin>>n;
    int* A = new int[n];
    cout<<"Введите элементы массива: ";
    for (int i = 0; i < n; ++i) { //цикл для создания рандомных элементов массива n размерностью 
        A[i] = rand() % ( n + n + 1 ) - n;
        cout << A[i] << " ";
    }
    MergeSort(A,0,n-1); //Вызов сортировки MergeSort()
    //Сначала мы передаем массив, начальный индекс = 0 и размер массива n
    cout<<"\n"<<"Отсортированный массив" <<endl;
    for(int i=0;i<n;i++) {
        cout<<A[i]<<" ";
    }
    delete[] A;
    return 0;
}
