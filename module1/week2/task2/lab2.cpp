#include <iostream>
using namespace std;
 
int main()
{
    setlocale (LC_ALL, "Russian");
    float a, eps;
    float xn, xn1 = 1;
    cout << "Введите число: " << endl;
    cin >> a; //ввод числа, из которого будет извлекаться корень
    cout << "Точность: " << endl; 
    cin >> eps; //для более приблежённого значения корня используется сравнение с введенным eps(точностью). Чем ближе введенное значение 0, тем точнее результат 
    while (abs(xn1-xn) >= eps) {
        xn = xn1; xn1 = (xn + a/xn)/2; //итерационная формула Герона
    }
    cout << "Корень из " << a << " = " << xn1 << endl;
    return 0;
}
