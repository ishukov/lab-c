//Эта программа запрашивает у пользователя имя файла, затем открывает файл с использованием класса ifstream и читает его содержимое построчно, выводя каждую строку на экран. Если файл не может быть открыт, программа выводит сообщение об ошибке.
#include <iostream>
#include <fstream>
using namespace std;

int main() {
    string fileName;
    cout << "Enter the file name: "; //1.txt
    cin >> fileName;

    ifstream file(fileName);
    if (file.is_open()) {
        string line;
        while (getline(file, line)) {
            cout << line << endl;
        }
        file.close();
    } else {
        cout << "Error opening file: " << fileName << endl;
    }

    return 0;
}
