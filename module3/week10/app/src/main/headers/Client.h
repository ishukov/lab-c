#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <string>
#include "Order.h"

class Client {
 public:
  Client(std::string name, std::string clientID, std::string email);
  void registerAccount();
  void placeOrder(Order &order);
  void makePayment(Order &order);
 private:
  std::string name_;
  std::string clientID_;
  std::string email_;
};

#endif // CLIENT_H
