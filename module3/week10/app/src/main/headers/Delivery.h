#ifndef DELIVERY_H
#define DELIVERY_H

#include <iostream>
#include <string>

class Delivery {
 public:
  Delivery(std::string deliveryID, std::string deliveryMethod, float deliveryCost, std::string pickupLocation);
 private:
  std::string deliveryID_;
  std::string deliveryMethod_;
  float deliveryCost_;
  std::string pickupLocation_;
};

#endif // DELIVERY_H
