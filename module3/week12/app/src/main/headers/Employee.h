#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>
#include <string>
#include "Person.h"
#include "Order.h"

using namespace std;

class Employee : public Person {
  public:
    Employee(string firstName, string lastName, string employeeId, string hireDate, double salary);
    string getEmployeeInfo();
    void setEmployeeId(string employeeId);
    string getEmployeeId();
    void setHireDate(string hireDate);
    string getHireDate();
    void setSalary(double salary);
    double getSalary();
  protected:
    string employeeId;
    string hireDate;
  private:
    double salary;
};


#endif // EMPLOYEE_H
