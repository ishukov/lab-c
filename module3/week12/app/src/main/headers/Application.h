#include <iostream>
#include "Frontend.h"
#include "Backend.h"

class Application {
    public:
        Application();
        Frontend& getFrontend();
        Backend& getBackend();
        // void welcoming_message();
        // void available_options();
        // int get_user_input();
    private:
        Frontend frontend_;
        Backend backend_;
};
