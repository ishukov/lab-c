#ifndef ORDER_H
#define ORDER_H

#include <iostream>
#include <string>
#include <vector>
#include "Product.h"

using namespace std;

class Order {
  public:
    Order(string orderId, string orderDate, string productId, int quantity, double totalPrice);
    string getOrderInfo();
    void setOrderId(string orderId);
    string getOrderId();
    void setOrderDate(string orderDate);
    string getOrderDate();
    void setProductId(string productId);
    string getProductId();
    void setQuantity(int quantity);
    int getQuantity();
    void setTotalPrice(double totalPrice);
    double getTotalPrice();
  private:
    string orderId;
    string orderDate;
    string productId;
    int quantity;
    double totalPrice;
};


#endif // ORDER_H
