#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <string>

using namespace std;

class Product {
  public:
    Product(string productId, string productName, double price);
    string getProductInfo();
    void setProductId(string productId);
    string getProductId();
    void setProductName(string productName);
    string getProductName();
    void setPrice(double price);
    double getPrice();
  private:
    string productId;
    string productName;
    double price;
};

#endif // ITEM_H

