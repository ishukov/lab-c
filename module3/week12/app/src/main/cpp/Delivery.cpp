#include "Delivery.h"
#include "PickupPoint.h"
#include "Order.h"


Delivery::Delivery(string deliveryId, string deliveryDate, string orderId, string deliveryAddress) {
    this->deliveryId = deliveryId;
    this->deliveryDate = deliveryDate;
    this->orderId = orderId;
    this->deliveryAddress = deliveryAddress;
}

string Delivery::getDeliveryInfo() {
    string info = "Delivery ID: " + deliveryId + "\n";
    info += "Order ID: " + orderId + "\n";
    info += "Delivery Address: " + deliveryAddress + "\n";
    return info;
}

void Delivery::setDeliveryId(string deliveryId) {
    this->deliveryId = deliveryId;
}

string Delivery::getDeliveryId() {
    return deliveryId;
}

void Delivery::setDeliveryDate(string deliveryDate) {
    this->deliveryDate = deliveryDate;
}

string Delivery::getDeliveryDate() {
    return deliveryDate;
}

void Delivery::setOrderId(string orderId) {
    this->orderId = orderId;
}

string Delivery::getOrderId() {
    return orderId;
}

void Delivery::setDeliveryAddress(string deliveryAddress) {
    this->deliveryAddress = deliveryAddress;
}

string Delivery::getDeliveryAddress() {
    return deliveryAddress;
}