#include "Order.h"
#include "Product.h"


Order::Order(string orderId, string orderDate, string productId, int quantity, double totalPrice) {
    this->orderId = orderId;
    this->orderDate = orderDate;
    this->productId = productId;
    this->quantity = quantity;
    this->totalPrice = totalPrice;
}

string Order::getOrderInfo() {
    string info = "Order ID: " + orderId + "\n";
    info += "Product ID: " + productId + "\n";
    info += "Quantity: " + to_string(quantity) + "\n";
    info += "Total Price: " + to_string(totalPrice) + "\n";
    return info;
}

void Order::setOrderId(string orderId) {
    this->orderId = orderId;
}

string Order::getOrderId() {
    return orderId;
}

void Order::setOrderDate(string orderDate) {
    this->orderDate = orderDate;
}

string Order::getOrderDate() {
    return orderDate;
}

void Order::setProductId(string productId) {
    this->productId = productId;
}

void Order::setQuantity(int quantity) {
    this->quantity = quantity;
}

int Order::getQuantity() {
    return quantity;
}

void Order::setTotalPrice(double totalPrice) {
    this->totalPrice = totalPrice;
}

double Order::getTotalPrice() {
    return totalPrice;
}

