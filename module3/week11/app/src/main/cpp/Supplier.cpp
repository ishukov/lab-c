#include "Supplier.h"

Supplier::Supplier(string firstName, string lastName, string supplierId, string companyName) : Person(firstName, lastName) {
    this->supplierId = supplierId;
    this->companyName = companyName;
}

string Supplier::getSupplierInfo() {
    string info = "Name: " + getFullName() + "\n";
    info += "Supplier ID: " + supplierId + "\n";
    info += "Company Name: " + companyName + "\n";
    return info;
}

void Supplier::setSupplierId(string supplierId) {
    this->supplierId = supplierId;
}

string Supplier::getSupplierId() {
    return supplierId;
}

void Supplier::setCompanyName(string companyName) {
    this->companyName = companyName;
}

string Supplier::getCompanyName() {
    return companyName;
}
