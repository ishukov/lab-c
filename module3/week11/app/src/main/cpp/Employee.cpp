#include "Employee.h"

Employee::Employee(string firstName, string lastName, string employeeId, string hireDate, double salary) : Person(firstName, lastName) {
    this->employeeId = employeeId;
    this->hireDate = hireDate;
    this->salary = salary;
}

string Employee::getEmployeeInfo() {
    string info = "Name: " + getFullName() + "\n";
    info += "Employee ID: " + employeeId + "\n";
    info += "Salary: " + to_string(salary) + "\n";
    return info;
}

void Employee::setEmployeeId(string employeeId) {
    this->employeeId = employeeId;
}

string Employee::getEmployeeId() {
    return employeeId;
}

void Employee::setHireDate(string hireDate) {
    this->hireDate = hireDate;
}

string Employee::getHireDate() {
    return hireDate;
}

void Employee::setSalary(double salary) {
    this->salary = salary;
}

double Employee::getSalary() {
    return salary;
}


