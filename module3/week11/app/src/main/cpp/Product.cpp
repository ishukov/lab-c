#include "Product.h"

Product::Product(string productId, string productName, double price) {
    this->productId = productId;
    this->productName = productName;
    this->price = price;
}

string Product::getProductInfo() {
    string info = "Product ID: " + productId + "\n";
    info += "Product Name: " + productName + "\n";
    info += "Price: " + to_string(price) + "\n";
    return info;
}

void Product::setProductId(string productId) {
    this->productId = productId;
}

string Product::getProductId() {
    return productId;
}

void Product::setProductName(string productName) {
    this->productName = productName;
}

string Product::getProductName() {
    return productName;
}

void Product::setPrice(double price) {
    this->price = price;
}

double Product::getPrice() {
    return price;
}