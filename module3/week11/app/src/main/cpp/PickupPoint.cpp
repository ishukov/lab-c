#include "PickupPoint.h"


PickupPoint::PickupPoint(std::string pickupPointID, std::string address, std::string phoneNumber) :
  pickupPointID_(pickupPointID), address_(address), phoneNumber_(phoneNumber) {}


std::string PickupPoint::getPickupPointID() {
  return pickupPointID_;
}

std::string PickupPoint::getAddress() {
  return address_;
}

std::string PickupPoint::getPhoneNumber() {
  return phoneNumber_;
}



