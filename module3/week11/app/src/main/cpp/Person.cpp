#include "Person.h"

Person::Person(string firstName, string lastName) {
    this->firstName = firstName;
    this->lastName = lastName;
}

string Person::getFullName() {
    return firstName + " " + lastName;
}

void Person::setFirstName(string firstName) {
    this->firstName = firstName;
}

string Person::getFirstName() {
    return firstName;
}

void Person::setLastName(string lastName) {
    this->lastName = lastName;
}

string Person::getLastName() {
    return lastName;
}
