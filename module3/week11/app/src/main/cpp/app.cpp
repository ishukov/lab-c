

#include "PickupPoint.h"

int main() {
  PickupPoint pickup1("PP001", "456 Park Ave.", "0987 6543");
  std::cout << "Pickup Point: " << pickup1.getAddress() << " (" << pickup1.getPhoneNumber() << ")" << std::endl;

  return 0;
}

