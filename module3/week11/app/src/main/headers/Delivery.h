#ifndef DELIVERY_H
#define DELIVERY_H

#include <iostream>
#include <string>

using namespace std;

class Delivery {
  public:
    Delivery(string deliveryId, string deliveryDate, string orderId, string deliveryAddress);
    string getDeliveryInfo();
    void setDeliveryId(string deliveryId);
    string getDeliveryId();
    void setDeliveryDate(string deliveryDate);
    string getDeliveryDate();
    void setOrderId(string orderId);
    string getOrderId();
    void setDeliveryAddress(string deliveryAddress);
    string getDeliveryAddress();
  private:
    string deliveryId;
    string deliveryDate;
    string orderId;
    string deliveryAddress;
};


#endif // DELIVERY_H
