#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>

using namespace std;

class Person {
  public:
    Person(string firstName, string lastName);
    string getFullName();
    void setFirstName(string firstName);
    string getFirstName();
    void setLastName(string lastName);
    string getLastName();
  private:
    string firstName;
    string lastName;
};

#endif // CLIENT_H