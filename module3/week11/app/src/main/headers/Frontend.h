#ifndef FRONTEND_H
#define FRONTEND_H

class Application;

class Frontend {
    public:
        Frontend(Application& app);
    private:
        Application& app_;
};

#endif
