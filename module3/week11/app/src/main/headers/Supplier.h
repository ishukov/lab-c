#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <iostream>
#include <string>
#include "Person.h"

using namespace std;

class Supplier : public Person {
  public:
    Supplier(string firstName, string lastName, string supplierId, string companyName);
    string getSupplierInfo();
    void setSupplierId(string supplierId);
    string getSupplierId();
    void setCompanyName(string companyName);
    string getCompanyName();
  protected:
    string supplierId;
    string companyName;
};


#endif // CUSTOMER_H
