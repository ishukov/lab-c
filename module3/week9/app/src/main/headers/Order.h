#ifndef ORDER_H
#define ORDER_H

#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

class Order {
 public:
  Order(std::string orderID);
  void addItem(Item &item);
  void removeItem(Item &item);
  void calculateTotalCost();
 private:
  std::string orderID_;
  std::vector<Item> items_;
  float totalCost_;
  std::string status_;
};

#endif // ORDER_H
