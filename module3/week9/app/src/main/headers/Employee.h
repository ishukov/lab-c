#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>
#include <string>
#include "Order.h"

class Employee {
 public:
  Employee(std::string name, std::string employeeID);
  void placeOrder(Order &order);
  void processPayment(Order &order);
  void shipOrder(Order &order);
 private:
  std::string name_;
  std::string employeeID_;
};

#endif // EMPLOYEE_H
