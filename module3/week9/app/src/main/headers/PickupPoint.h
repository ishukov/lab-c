#ifndef PICKUPPOINT_H
#define PICKUPPOINT_H

#include <iostream>
#include <string>

class PickupPoint {
  public:
    PickupPoint(std::string pickupPointID, std::string address, std::string phoneNumber);
    std::string getPickupPointID();
    std::string getAddress();
    std::string getPhoneNumber();
  private:
    std::string pickupPointID_;
    std::string address_;
    std::string phoneNumber_;
};

#endif // PICKUPPOINT_H
