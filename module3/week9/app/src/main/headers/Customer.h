#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <iostream>
#include <string>
#include "Order.h"

class Customer {
 public:
  Customer(std::string name, std::string customerID);
  void placeOrder(Order &order);
  void makePayment(Order &order);
 private:
  std::string name_;
  std::string customerID_;
};

#endif // CUSTOMER_H
