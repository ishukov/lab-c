#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <string>

class Item {
 public:
  Item(std::string name, double price, int quantity);
  std::string getName();
  double getPrice();
  int getQuantity();
 private:
  std::string name_;
  double price_;
  int quantity_;
};

#endif // ITEM_H

