

#include "PickupPoint.h"

int main() {
  
  PickupPoint pickup1("PP001", "456 Park Ave.", "0987 6543");
  std::cout << "Pickup Point: " << pickup1.getAddress() << " (" << pickup1.getPhoneNumber() << ")" << std::endl;
  // std::string pickupPointID, address, phoneNumber;

  // // std::cout << "Enter pickup point ID: ";
  // // std::cin >> pickupPointID;

  // std::cout << "Enter pickup point address: " << std::endl;
  // std::cin >> address;

  // std::cout << "Enter pickup point phone number: " << std::endl;
  // std::cin >> phoneNumber;

  // PickupPoint pickup1(pickupPointID, address, phoneNumber);
  // std::cout << "Pickup Point: " << pickup1.getAddress() << " (" << pickup1.getPhoneNumber() << ")" << std::endl;
  return 0;
}

