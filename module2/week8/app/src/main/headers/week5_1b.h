#ifndef WEEK51B_H
#define WEEK51B_H

#include <map>
#include <set>
#include <vector>
#include <string>

using namespace std;

class LangTest{
    public:
    static pair<int, set<string>> countLangs(int n, vector<vector<string>>& students){
        set<string> all, atleast;
        for (auto student : students) {
            for (auto lang : student) {
                atleast.insert(lang);
                all.insert(lang);
            }
        }
        for (auto student : students) {
            set<string> temp;
            for (auto lang : student) {
                temp.insert(lang);
            }
            for (auto lang : all) {
                if (!temp.count(lang)) {
                    all.erase(lang);
                }
            }
        }
        return make_pair(all.size(), all);
    }
};

#endif