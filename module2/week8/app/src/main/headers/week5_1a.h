#ifndef WEEK51A_H
#define WEEK51A_H

#include <iostream>
#include <set>
#include <map>
#include <vector>

using namespace std;

class RemoveDuplicates {
    public:
        static void remove(std::vector<int>& nums) {
            std::set<int> unique;
            for (auto num : nums) {
                unique.insert(num);
            }
            nums.assign(unique.begin(), unique.end());
        }
};


#endif