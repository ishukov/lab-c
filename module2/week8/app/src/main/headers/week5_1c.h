#ifndef WEEK51C_H
#define WEEK51C_H

#include <string>
#include <stack>
#include <stdlib.h>


using namespace std;

class Solution {
public:
    bool isValid(std::string s) {
        std::stack<char> brackets;
        for (char c : s) {
            switch (c) {
                case '(':
                case '{':
                case '[':
                    brackets.push(c);
                    break;
                case ')':
                    if (brackets.empty() || brackets.top() != '(') return false;
                    else brackets.pop();
                    break;
                case '}':
                    if (brackets.empty() || brackets.top() != '{') return false;
                    else brackets.pop();
                    break;
                case ']':
                    if (brackets.empty() || brackets.top() != '[') return false;
                    else brackets.pop();
                    break;
            }
        }
        return brackets.empty();
    }
};

#endif


