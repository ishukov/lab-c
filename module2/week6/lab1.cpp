#include <iostream>
#include <unordered_map>
using namespace std;

pair<char, int> mostFrequent(string s) {
    unordered_map<char, int> charCount;
    for (char c : s) {
        charCount[c]++;
    }
    char maxChar = ' ';
    int maxCount = 0;
    for (auto [c, count] : charCount) {
        if (count >= maxCount) {
            maxChar = c;
            maxCount = count;
        }
    }
    return {maxChar, maxCount};
}

int main() {
    string s = "aaa bbb cc";
    auto [mostFrequentChar, count] = mostFrequent(s);
    cout << "Дано: " << s << endl;
    cout << "Результат: " << "[" << mostFrequentChar << "," << count << "]" << endl;
    return 0;
}
