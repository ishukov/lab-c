plugins {
    id("cpp-application")
}

// Определяем путь к корню проекта (week7)
val projectRoot = projectDir.parentFile.parentFile

application {
    source.from(file("src/main/cpp"))
    targetMachines.add(machines.linux.x86_64)
}

tasks.withType<CppCompile>().configureEach {
    // Enable C++17
    compilerArgs.add("-std=c++17")
    
    // Add necessary compiler flags for Boost
    compilerArgs.add("-DBOOST_IOSTREAMS_NO_LIB")
    
    // Include directories for Linux
    includes.from(
        file("/usr/include"),                    // System headers
        file("/usr/include/boost"),              // Boost headers
        file("/usr/local/include"),              // Local headers
        file("src/main/headers"),                // Headers directory
        file("${projectRoot}/libs/gnuplot/gnuplot-iostream")  // Gnuplot-iostream headers
    )
}

tasks.withType<LinkExecutable>().configureEach {
    // Add library search paths
    linkerArgs.add("-L/usr/lib")
    linkerArgs.add("-L/usr/local/lib")
    
    // Required libraries
    linkerArgs.add("-lboost_iostreams")
    linkerArgs.add("-lboost_system")
    linkerArgs.add("-lpthread")
}

// Create a custom task to run the application
tasks.register<Exec>("runApp") {
    dependsOn("linkDebug")
    
    // The executable will be in the build/exe/main/debug directory
    executable = "${buildDir}/exe/main/debug/${project.name}"
    
    // Ensure the working directory is set correctly
    workingDir = projectDir
}