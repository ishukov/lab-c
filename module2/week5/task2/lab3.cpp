#include <iostream>
#include <map>
#include <chrono>
#include <time.h>

using namespace std;
using namespace std::chrono;


map<int, unsigned long long> fib_map;

unsigned long long fibonacci(int n) {
    if (fib_map.count(n) == 0) {
        if (n <= 2) {
            fib_map[n] = 1;
        } else {
            fib_map[n] = fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
    return fib_map[n];
}

map<int, long long> fib_cache;

long long fibonacci_optimized(int n) {
    if (n <= 0) return 0;
    if (n == 1) return 1;

    if (fib_cache.count(n) > 0) {
        return fib_cache[n];
    }

    fib_cache[n] = fibonacci_optimized(n-1) + fibonacci_optimized(n-2);
    return fib_cache[n];
}

int main() {
    int n;
    cout << "Enter n: ";
    cin >> n;
    cout << "Classic Fibonacci: " << fibonacci(n) << endl;
    cout << "Optimized Fibonacci: " << fibonacci_optimized(n)<< endl;
    clock_t start = clock();
    clock_t end = clock();
    double time = (double)(end - start) / CLOCKS_PER_SEC;
    std::cout << "Implementation time: " << time << " second" << endl;
    return 0;
}

