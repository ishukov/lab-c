#include <iostream>
#include <vector>
using namespace std;

int maxWater(vector<int> heights) {
    int left = 0, right = heights.size() - 1;
    int maxWater = 0;
    while (left < right) {
        int width = right - left;
        int height = min(heights[left], heights[right]);
        maxWater = max(maxWater, width * height);
        if (heights[left] < heights[right]) {
            left++;
        } else {
            right--;
        }
    }
    return maxWater;
}

int main() {
    int n;
    vector<int> heights;
    cout << "Enter the number of lines: ";
    cin >> n;
    cout << "Enter the heights of each line separated by a space:";
    for(int i = 0; i < n; i++) {
        int height;
        cin >> height;
        heights.push_back(height);
    }
    cout << "Maximum water area: " << maxWater(heights) << endl;
    return 0;
}
