#include <iostream>
#include <vector>
#include <algorithm>  // std::next_permutation, std::sort

using namespace std;

vector<vector<int>> perms(vector<int> nums) {
    vector<vector<int>> result;
    sort(nums.begin(), nums.end());
    do {
        result.push_back(nums);
    } 
    while (next_permutation(nums.begin(), nums.end()));
    return result;
}

int main() {
    vector<int> nums = {1, 2, 3};
    cout << "[";
    for (const auto& i: nums) {
        std::cout << i << ", ";
    }
    cout << "\b\b]" << endl;
    vector<vector<int>> result = perms(nums);
    cout << "[";
    for (auto perm : result) {
        cout << "[";
            for (auto num : perm) {
                cout << num << ", ";
            }
        cout << "\b\b] ";
    }
    cout << "\b\b]" <<endl;
    return 0;
}


