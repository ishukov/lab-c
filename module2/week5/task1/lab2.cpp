#include <iostream>
#include <climits>
#include <vector>

using namespace std;

int maxProfit(vector<int> prices) {
    int minPrice = INT_MAX;
    int maxProfit = 0;
    for (int i = 0; i < prices.size(); i++) {
        if (prices[i] < minPrice) {
            minPrice = prices[i];
        } else if (prices[i] - minPrice > maxProfit) {
            maxProfit = prices[i] - minPrice;
        }
    }
    return maxProfit;
}

int main() {
    int n;
    vector<int> prices;
    cout << "Enter the number of days: ";
    cin >> n;
    cout << "Enter the prices for each day: ";
    for(int i = 0; i < n; i++) {
        int price;
        cin >> price;
        prices.push_back(price);
    }
    cout << "Max profit: " << maxProfit(prices) << endl;
    return 0;
}
