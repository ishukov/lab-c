#include <iostream>
#include <set>
using namespace std;

int main() {
    int n;
    cin >> n; //6; 1 1 2 2 3 3
    set<int> s;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        s.insert(x);
    }
    for (auto x : s) {
        cout << x << " "; 
    }
    cout << endl;
    return 0;
}
