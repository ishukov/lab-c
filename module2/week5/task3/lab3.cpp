#include <iostream>
#include <stack>

using namespace std;

bool isValid(string s) {
    stack<char> str;
    for (char c : s) {
        if (c == '(' || c == '{' || c == '[') {
        str.push(c);
        } else {
            if (str.empty()) {
                return false;
            }
            char top = str.top();
            str.pop();
            if ((c == ')' && top != '(') || (c == '}' && top != '{') || (c == ']' && top != '[')) {
                return false;
            }
        }
    }
    return str.empty();
}

int main() {
    string input = "([]{})";
    //string input = "([]{}";
    if (isValid(input)) {
        cout << "Input is valid" << endl;
    } else {
        cout << "Input is not valid" << endl;
    }
return 0;
}
