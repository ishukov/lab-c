#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <fstream>

using namespace std;

set<string> all_languages, temp;

void calculate_languages(int students_num) {
    for (int i = 0; i < students_num; i++) {
        int languages_num;
        cin >> languages_num;
        set<string> student_languages;
        for (int j = 0; j < languages_num; j++) {
            string language;
            cin >> language;
            student_languages.insert(language);
        }
        if (i == 0) {
            all_languages = student_languages;
            temp = student_languages;
        } else {
            set<string> intersection;
            set_intersection(all_languages.begin(), all_languages.end(), student_languages.begin(), student_languages.end(), inserter(intersection, intersection.begin()));
            all_languages = intersection;
            set_union(temp.begin(), temp.end(), student_languages.begin(), student_languages.end(), inserter(temp, temp.begin()));
        }
    }
}

int main() {
    int students_num;
    cin >> students_num;
    calculate_languages(students_num);
    cout << all_languages.size() << endl;
    for (string language : all_languages) {
        cout << language << endl;
    }
    cout << temp.size() << endl;
    for (string language : temp) {
        cout << language << endl;
    }
    return 0;
}

// cd "/workspace/lab_cpp/module2/week5/task3/" && g++ lab2.cpp -o lab2 && "/workspace/lab_cpp/module2/week5/task3/"lab2 < data.txt
