#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>

using namespace std;

class Person {
    public:
        int getId();
        std::string getName();
        void setId(int id);
        void setName(std::string name);
    private:
        int id;
        std::string name;
};



#endif // CLIENT_H