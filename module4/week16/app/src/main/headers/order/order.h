#ifndef ORDER_H
#define ORDER_H

#include <iostream>
#include "Order.h"
#include <string>
#include <vector>

class Order {
    public:
        virtual std::string getOrderInfo() = 0;
        virtual double getTotalPrice() = 0;
        virtual setOrderId(string orderId) = 0;
};

#endif // ORDER_H
