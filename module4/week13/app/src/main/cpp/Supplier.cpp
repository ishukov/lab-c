#include "Supplier.h"


string Supplier::getSupplierInfo() {
    string info = "Name: " + getName() + "\n";
    info += "Supplier ID: " + supplierId + "\n";
    info += "Company Name: " + companyName + "\n";
    return info;
}

void Supplier::setSupplierId(string supplierId) {
    this->supplierId = supplierId;
}

string Supplier::getSupplierId() {
    return supplierId;
}

void Supplier::setCompanyName(string companyName) {
    this->companyName = companyName;
}

string Supplier::getCompanyName() {
    return companyName;
}
