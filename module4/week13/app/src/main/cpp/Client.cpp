#include "Client.h"

Client::Client(string firstName, string lastName, string clientId, string email) : Person(firstName, lastName) {
    this->clientId = clientId;
    this->email = email;
}

string Client::getClientInfo() {
    string info = "Name: " + getName() + "\n";
    info += "Client ID: " + clientId + "\n";
    info += "Email: " + email + "\n";
    return info;
}

void Client::setClientId(string clientId) {
    this->clientId = clientId;
}

string Client::getClientId() {
    return clientId;
}

void Client::setEmail(string email) {
this->email = email;
}

string Client::getEmail() {
return email;
}
