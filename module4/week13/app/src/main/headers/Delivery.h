#ifndef DELIVERY_H
#define DELIVERY_H

#include <iostream>
#include <string>

using namespace std;

class Delivery {
    public:
        DeliveryAll(string deliveryInfo, string deliveryAddress, string deliveryDate);
        void setDeliveryInfo(std::string deliveryInfo); 
        string getDeliveryInfo();
        void setDeliveryAddress(string deliveryAddress);
        string getDeliveryAddress();
        void setDeliveryDate(string deliveryDate);
        string getDeliveryDate();
    
    private:
        string deliveryInfo;
        string deliveryAddress;
        string deliveryDate;
};

#endif // DELIVERY_H
