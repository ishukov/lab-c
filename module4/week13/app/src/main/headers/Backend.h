#ifndef BACKEND_H
#define BACKEND_H

#include <iostream>

class Application;

class Backend {
    public:
        Backend(Application& app);
    private:
        Application& app_;
};

#endif
