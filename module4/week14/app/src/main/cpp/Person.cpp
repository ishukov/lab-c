#include "Person.h"

int Person::getId() {
    return id;
}

std::string Person::getName() {
    return name;
}

void Person::setId(int id) {
    this->id = id;
}

void Person::setName(std::string name) {
    this->name = name;
}
