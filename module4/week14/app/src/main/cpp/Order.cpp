#include "Order.h"


void Order::setOrderId(string orderId) {
    this->orderId = orderId;
}

string Order::getOrderId() {
    return orderId;
}

void Order::setOrderDate(string orderDate) {
    this->orderDate = orderDate;
}

string Order::getOrderDate() {
    return orderDate;
}

void Order::setProductId(string productId) {
    this->productId = productId;
}

void Order::setQuantity(int quantity) {
    this->quantity = quantity;
}

int Order::getQuantity() {
    return quantity;
}

void Order::setTotalPrice(double totalPrice) {
    this->totalPrice = totalPrice;
}

double Order::getTotalPrice() {
    return totalPrice;
}

