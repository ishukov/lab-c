#include "Delivery.h"

string Delivery::DeliveryAll() {
    string info = "Delivery Info: " + deliveryInfo + "\n";
    info += "Delivery Address: " + deliveryAddress + "\n";
    info += "Delivery Data: " + deliveryDate + "\n";
    return info;
}

void Delivery::setDeliveryInfo(string DeliveryInfo) {
    this->deliveryInfo = DeliveryInfo;
}

string Delivery::getDeliveryInfo() {
    return DeliveryInfo;
}

void Delivery::setDeliveryDate(string deliveryDate) {
    this->deliveryDate = deliveryDate;
}

string Delivery::getDeliveryDate() {
    return deliveryDate;
}

void Delivery::setDeliveryAddress(string deliveryAddress) {
    this->deliveryAddress = deliveryAddress;
}

string Delivery::getDeliveryAddress() {
    return deliveryAddress;
}
