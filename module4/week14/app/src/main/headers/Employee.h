#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>
#include <string>
#include "Person.h"


class Employee: public Person {
    public:
        std::string getPosition();
        double getSalary();
        void setPosition(std::string position);
        void setSalary(double salary);
    private:
        std::string position;
        double salary;
};



#endif // EMPLOYEE_H
