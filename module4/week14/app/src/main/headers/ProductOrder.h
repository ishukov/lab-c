#ifndef PRODUCTORDER_H
#define  PRODUCTORDER_H

#include "Order.h"
#include <iostream>
#include <string>
#include <vector>

class ProductOrder : public Order {
    public:
        std::vector<std::string> getProductList();
        void setProductList(std::vector<std::string> productList);
        std::string getOrderInfo() override;
        double getTotalPrice() override;
    
    private:
        std::vector<std::string> productList_;
};

#endif //  PRODUCTORDER_H
