#ifndef SERVICEORDER_H
#define SERVICEORDER_H

#include "Order.h"
#include <iostream>
#include <string>
#include <vector>


class ServiceOrder : public Order {
  public:
      std::vector<std::string> getServiceList();
      void setServiceList(std::vector<std::string> serviceList);
      std::string getOrderInfo() override;
      double getTotalPrice() override;
      
  private:
      std::vector<std::string> serviceList_;
};

#endif // SERVICEORDER_H