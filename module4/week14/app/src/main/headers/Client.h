#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <string>
#include "Order.h"
#include "Person.h"

using namespace std;

class Client : public Person {
  public:
    Client(string firstName, string lastName, string clientId, string email);
    string getClientInfo();
    void setClientId(string clientId);
    string getClientId();
    void setEmail(string email);
    string getEmail();
  protected:
    string clientId;
    string email;
};


#endif // CLIENT_H
