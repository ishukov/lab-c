#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <list>

#include "StackInterface.h"

class RealStack: IntegerStack {
private:
    std::list<int> stack = {};
    int limit = 42;
public:
    RealStack(std::list<int> stack);
    RealStack();

    virtual bool isEmpty();
    virtual int size();
    virtual void push(int number);
    virtual int pop();

    const int &getLimit() const { return this->limit; };
    void setLimit(const int &newlimit) { this->limit = newlimit; };
};

#endif // STACK_H
