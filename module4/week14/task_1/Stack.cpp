#include "Stack.h"

RealStack::RealStack() = default;
RealStack::RealStack(std::list<int> stack) { 
    this->stack = stack; 
};

bool RealStack::isEmpty() { 
    return this->stack.empty(); 
};

int RealStack::size() { 
    return this->stack.size(); 
};
void RealStack::push(int number){
    this->stack.push_back(number);
    if (this->stack.size() > this->limit) {
        this->pop();
    }
};

int RealStack::pop() { 
    int poped_number = this->stack.front();
    this->stack.pop_front(); 
    return poped_number;
};
