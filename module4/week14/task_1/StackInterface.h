#ifndef STACKINTERFACE_H
#define STACKINTERFACE_H

#include <iostream>

class InterfaceStack {
public:
    virtual bool isEmpty() = 0;
    virtual int size() = 0;
    virtual void push(int number) = 0;
    virtual int pop() = 0;
};

#endif // STACKINTERFACE_H
