#include <iostream>

using namespace std;

template <class T> void merge(T container, int left, int mid, int right) {
    int left_container_length = mid - left + 1;
    int right_container_length = right - mid;
 
    int *left_container = new int[left_container_length];
    int *right_container = new int[right_container_length];
 
    for (int i = 0; i < left_container_length; i++)
        left_container[i] = container[left + i];
    for (int j = 0; j < right_container_length; j++)
        right_container[j] = container[mid + 1 + j];
 
    int left_arr_index = 0;
    int right_arr_index = 0;
    int merged_arr_index = left;
 
    while (left_arr_index < left_container_length && right_arr_index < right_container_length) {
        if (left_container[left_arr_index] <= right_container[right_arr_index]) {
            container[merged_arr_index] = left_container[left_arr_index];
            left_arr_index++;
        }
        else {
            container[merged_arr_index] = right_container[right_arr_index];
            right_arr_index++;
        }
        merged_arr_index++;
    }
    while (left_arr_index < left_container_length) {
        container[merged_arr_index] = left_container[left_arr_index];
        left_arr_index++;
        merged_arr_index++;
    }
    while (right_arr_index < right_container_length) {
        container[merged_arr_index] = right_container[right_arr_index];
        right_arr_index++;
        merged_arr_index++;
    }
    delete[] left_container;
    delete[] right_container;
}
 
template <class T> void mergesort(T container, int begin, int end)
{
    if (begin >= end)
        return;
 
    int mid = begin + (end - begin) / 2;
    mergesort(container, begin, mid);
    mergesort(container, mid + 1, end);
    merge(container, begin, mid, end);
}
 
template <class T> void printcontainer(T container, int container_length)
{
    for (int i = 0; i < container_length; i++)
        cout << container[i] << " ";
}
 
int main() {
    int container_length;
    cout << "Укажите размер массива: ";
    cin >> container_length;
    cout << "Заполните массив." << endl;
    double *container = new double[container_length];
    	for (int i = 0; i < container_length; i++) {
            cin >> container[i];
	  }
    mergesort(container, 0, container_length - 1);
 
    printcontainer(container, container_length);

    delete [] container;

    cout << endl;
    
    return 0;
}
